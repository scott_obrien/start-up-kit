/* ••••••••• Custom JS ••••••••• */
$(document).ready(function() {

	// ••••••••• Radio / Checkboxes •••••••••
	$('input:checkbox').change(function() {
		if ($(this).is(':checked')) {
			$(this).parent().addClass('checked');
		} else {
			$(this).parent().removeClass('checked');
		}
	});

	$('input:radio').click(function() {
		// Remove the 'checked' class from all parent labels in the radio button group, before turning on the clicked one
		var radioName = $(this).attr('name');
		$('input:radio[name='+radioName+']').parent().removeClass('checked');
		$(this).parent().addClass('checked');

	});

	$('input:radio, input:checkbox').focusin(function() {
		$(this).parent().addClass('hasfocus');
	});

	$('input:radio, input:checkbox').focusout(function() {
		$(this).parent().removeClass('hasfocus');
	});


	var inputDisabled = $('input:disabled');

	if ( $(inputDisabled) ) {
		$(inputDisabled).parent().addClass('disabled');
	}




	// ••••••••• Select Dropdowns •••••••••
	$(function(){
	var $selects = $('select');
						
	$selects.easyDropDown({
			cutOff: 10,
			wrapperClass: 'my-dropdown-class',
			/*onChange: function(selected){
				// do something
			}*/
		});
	});




});