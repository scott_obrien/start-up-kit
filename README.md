### README ####

Basic Start-Up Kit for Seamgen's front-end web development team.

**Contains:**
* Bootstrap 3.2.0 / Sass Environment ready
* Selects, Dropdowns
* Check Boxes
* Radio's
* Generic Buttons